from django.urls import path
from receipts.views import show_receipt, create_receipt, show_categories, show_accounts, create_category, create_account


urlpatterns = [
    path("accounts/create/", create_account, name='create_account'),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/", show_accounts, name="account_list"),
    path("categories/", show_categories, name="category_list"),
    path("create/", create_receipt, name="create_receipt"),
    path("", show_receipt, name="home"),
]
