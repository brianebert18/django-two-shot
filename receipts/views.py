from django.shortcuts import render, redirect, get_list_or_404
from receipts.models import Receipt, Account, ExpenseCategory, User
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm
from django.db.models import Count




# Create your views here.
@login_required
def show_receipt(request):
    user = request.user
    receipts = Receipt.objects.filter(purchaser=user)

    context = {
        "receipts": receipts,
    }
    return render(request, "receipt.html", context)

@login_required
def create_receipt(request):
    if request.method == "POST":
        form = ReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = ReceiptForm()
    context = {
        "form" : form,
    }
    return render(request, "create.html", context)


@login_required
def show_categories(request):
        user = request.user
        categories = ExpenseCategory.objects.filter(owner=user).annotate(num_receipts=Count('receipts'))

        context = {
             "categories" : categories,
        }

        return render(request, "categories.html", context)

@login_required
def show_accounts(request):
     user = request.user
     accounts = Account.objects.filter(owner=user).annotate(num_receipts=Count('receipts'))

     context = {
          "accounts" : accounts
     }
     return render(request, "accounts.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CategoryForm(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect("category_list")
    else:
        form = CategoryForm()

    context = {
        "form": form,
    }
    return render(request, "create_category.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = AccountForm(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect("account_list")
    else:
        form = AccountForm()

    context = {
        "form" : form,
    }
    return render(request, "create_account.html", context)
